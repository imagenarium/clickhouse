#!/bin/bash

if [[ "${EMPTY_DATA}" == "true" ]]; then
  cp -r /prepared_data/clickhouse-server /etc

  echo "[IMAGENARIUM]: Enable overcommit memory..."
  sysctl -w vm.overcommit_memory=1
fi

exec /entrypoint.sh "$@"
