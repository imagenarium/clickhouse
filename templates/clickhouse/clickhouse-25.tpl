<@requirement.NODE 'clickhouse' 'clickhouse-${namespace}' 'clickhouse-standby-${namespace}' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' value='8123' type='port' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />

<@requirement.PARAM name='CLICKHOUSE_DB' value='default' />
<@requirement.PARAM name='CLICKHOUSE_USER' value='default' />
<@requirement.PARAM name='CLICKHOUSE_PASSWORD' />

<@requirement.PARAM name='VIRTUAL_VOLUME' value='false' type='boolean' />
<@requirement.PARAM name='VIRTUAL_VOLUME_SIZE_MB' values='128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576' value='1024' type='select' depends='VIRTUAL_VOLUME' />

<@img.TASK 'clickhouse-${namespace}' 'imagenarium/clickhouse:25.1'>
  <@img.NODE_REF 'clickhouse' />
  <@img.PORT PARAMS.PUBLISHED_PORT '8123' PARAMS.PUBLISHED_PORT_TYPE />

  <@img.BIND '/dev/shm' '/dev/shm' />

  <@img.ULIMIT 'nofile=262144:262144' />
  <@img.ULIMIT 'nproc=4096:4096' />
  <@img.ULIMIT 'memlock=-1:-1' />

  <@img.CUSTOM '--privileged=true' />

  <@img.VOLUME '/var/lib/clickhouse' PARAMS.VIRTUAL_VOLUME?boolean?then('virtual','local') PARAMS.VIRTUAL_VOLUME_SIZE_MB />
  <@img.VOLUME '/etc/clickhouse-server' PARAMS.VIRTUAL_VOLUME?boolean?then('virtual','local') '128' />
  <@img.VOLUME '/var/log/clickhouse-server' PARAMS.VIRTUAL_VOLUME?boolean?then('virtual','local') '128' />

  <@img.ENV 'CLICKHOUSE_DB' PARAMS.CLICKHOUSE_DB />
  <@img.ENV 'CLICKHOUSE_USER' PARAMS.CLICKHOUSE_USER />
  <@img.ENV 'CLICKHOUSE_PASSWORD' PARAMS.CLICKHOUSE_PASSWORD />

  <@img.CHECK_PATH ':8123/ping' />
</@img.TASK>
